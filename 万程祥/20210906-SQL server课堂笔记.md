# 20210906-SQL server课堂笔记

## 1、 ROW_NUMBER
* 说明：返回结果集分区内行的序列号，每个分区的第一行从1开始。
* 语法：ROW_NUMBER () OVER  ([ <partition_by_clause> ] <order_by_clause>) 。
* 备注：ORDER BY 子句可确定在特定分区中为行分配唯一 ROW_NUMBER 的顺序。
* 参数：<partition_by_clause> ：将 FROM 子句生成的结果集划入应用了 ROW_NUMBER 函数的分区。
      <order_by_clause>：确定将 ROW_NUMBER 值分配给分区中的行的顺序。
* 返回类型：bigint 。
* Row_number的用途非常广泛，排序最好用他，一般可以用来实现web分页，他会为查询出来的每一页记录生成一个序号，依次排序且不重复，注意使用row_number函数时必须要用over子句选择对某一列进行排序才能生成序号。
--------------------------------------
* over子句中根据datatime降序排列，Sql语句则按salse降序排列
--------------------------------------
* 利用row_number可以实现web程序分页

> with orderSection as
(
select ROW_NUMBER() OVER(order by salse desc) rownum,* 
from rank1
)
select * from [orderSection] where rownum between 5 and 8
 order by salse desc
--------------------------------------
* 下面我们写一个例子来证实这一点，将上面Sql语句中的排序字段由salse改为datatime。
例如：

>with orderSection as
(
select ROW_NUMBER() OVER(order by salse desc) rownum,*
 from rank1
)
select * from [orderSection] where rownum between 5 and 8 
order by datatime desc


## 2、 RANK
* rank函数用于返回结果集的分区内每行的排名， 行的排名是相关行之前的排名数加一。
简单来说rank函数就是对查询出来的记录进行排名，与row_number函数不同的是，rank函数考虑到了over子句中排序字段值相同的情况，
如果使用rank函数来生成序号，over子句中排序字段值相同的序号是一样的，后面字段值不相同的序号将跳过相同的排名号排下一个，也就是相关行之前的排名数加一，
可以理解为根据当前的记录数生成序号，后面的记录依此类推。
* RANK()函数的语法如下所示：
> RANK() OVER (
    [PARTITION BY partition_expression, ... ]
    ORDER BY sort_expression [ASC | DESC], ...
)
* 在这个语法中：
> 首先，PARTITION BY子句划分应用该函数的结果集分区的行。其次，ORDER BY子句指定应用该函数每个分区中行的逻辑排序顺序。


## 3、DENSE_RANK
* dense_rank函数的功能与rank函数类似，dense_rank函数在生成序号时是连续的，而rank函数生成的序号有可能不连续。dense_rank函数出现相同排名时，将不跳过相同排名号，rank值紧接上一次的rank值。在各个分组内，rank()是跳跃排序，有两个第一名时接下来就是第四名，dense_rank()是连续排序，有两个第一名时仍然跟着第二名。将上面的Sql语句改由dense_rank函数来实现



