# SQL server 分页知识点


 offset A rows ,将前A条记录舍去，fetch next B rows only ，向后在读取B条数据。




``` sql 
declare @pageIndex int =1
declare @pagSize int=10

select * from StudentInfo
order by Id
offset (@pageIndex-1)*@pagSize rows fetch next @pagSize rows only
```


```
第一种：ROW_NUMBER() OVER()方式

把表中的所有数据都按照一个ROW_NUMBER进行排序，然后查询ROW_NUMBER 10 到20之间的前十条记录。

SELECT * FROM ( 

　　　　SELECT *, ROW_NUMBER() OVER(ORDER BY LOG_ID ) AS ROWID FROM LOG_SYSTEM

　　) AS B

WHERE ROWID BETWEEN 10 AND 20 

---WHERE ROWID BETWEEN 当前页数-1*条数 AND 页数*条数---     

```


```
第二种方式：OFFSET FETCH NEXT方式（SQL2012以上的版本才支持：推荐使用 ）

使用OFFSET是SQLServer2012新具有的分页功能，主要功能是从第x条数据开始共取y数据。但是其必须根再Order By后面使用，相比前三种方式更加方便。

SELECT * FROM LOG_SYSTEM 

ORDER BY LOG_ID 

OFFSET 4 ROWS FETCH NEXT 5 ROWS ONLY

---ORDER BY LOG_ID 

   OFFSET 页数 ROWS FETCH NEXT 条数 ROWS ONLY ---
```

```
--第三种方式：--TOP NOT IN方式 （适应于数据库2012以下的版本）

先搜出id在1-15之间的数据，紧接着搜出id不在1-15之间的数据，最后将搜出的结果取前十条。

SELECT TOP 10 * FROM LOG_SYSTEM 

WHERE LOG_ID NOT IN (SELECT TOP 15 LOG_ID FROM LOG_SYSTEM)

---WHERE ID NOT IN (SELECT TOP 条数*页数  LOG_ID  FROM LOG_SYSTEM)  ---
```



```
--第四种方式：用存储过程的方式进行分页 

CREATE PROCEDURE PAGE_DEMO

@TABLENAME VARCHAR(20),

@PAGESIZE INT,

@PAGE INT

AS

DECLARE @NEWSPAGE INT,

@RES VARCHAR(100)

BEGIN

SET @NEWSPAGE=@PAGESIZE*(@PAGE - 1)

SET @RES='SELECT * FROM ' +@TABLENAME+ ' ORDER BY LOG_ID OFFSET '+CAST(@NEWSPAGE AS VARCHAR(10)) +' ROWS FETCH NEXT '+ CAST(@PAGESIZE AS VARCHAR(10)) +' ROWS ONLY'

EXEC(@RES)

END

EXEC PAGE_DEMO @TABLENAME='LOG_SYSTEM',@PAGESIZE=3,@PAGE=5

GO

```