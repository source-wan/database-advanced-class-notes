# 20210907-SQL server排序

## 题目-解答

    -- 15.1 按各科成绩进行排序，并显示排名， Score 重复时合并名次
    select * ,DENSE_RANK() over(partition by CourseId order by Score desc)
    from StudentCourseScore

    -- 16.查询学生的总成绩，并进行排名，总分重复时保留名次空缺
    select StudentId , sum(Score)总分 , RANK() over(order by sum(Score) desc) 排名
    from StudentCourseScore 
    group by StudentId

    -- 16.1 查询学生的总成绩，并进行排名，总分重复时不保留名次空缺
    select StudentId , sum(Score) , Row_number() over(order by sum(Score) desc)
    from StudentCourseScore
    group by StudentId

    -- 17.统计各科成绩各分数段人数：课程编号，课程名称，
    -- [100-85]，[85-70]，[70-60]，[60-0] 及所占百分比

    select CourseId , sum(a1)[100-85],sum(a2) [85-70],sum(a3)[70-60],sum(a4) [60-0],
    sum(a1)+sum(a2)+sum(a3)+sum(a4)总数,
    convert (nvarchar(50),(convert (decimal(18,2) , sum(a1)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%'[100-85],
    convert (nvarchar(50),(convert (decimal(18,2) ,sum(a2)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%' [85-70],
    convert (nvarchar(50),(convert (decimal(18,2) , sum(a3)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%'[70-60],
    convert (nvarchar(50),(convert (decimal(18,2) , sum(a4)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%'[60-0]
    from
    (
	select  CourseId ,
	(case when Score>=85 then 1 else 0 end) a1,
	(case when Score<85 and Score >=70  then 1 else 0 end) a2,
	(case when Score<70 and Score >=60  then 1 else 0 end) a3,
	(case when Score<60 and Score >=0  then 1 else 0 end) a4
	from StudentCourseScore
	where CourseId=1
    ) a
    group by CourseId
    union 
    select CourseId , sum(a1)[100-85],sum(a2) [100-85],sum(a3)[100-85],sum(a4) [100-85],
    sum(a1)+sum(a2)+sum(a3)+sum(a4)总数,
    convert (nvarchar(50),(convert (decimal(18,2) , sum(a1)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%',
    convert (nvarchar(50),(convert (decimal(18,2) ,sum(a2)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%',
    convert (nvarchar(50),(convert (decimal(18,2) , sum(a3)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%',
    convert (nvarchar(50),(convert (decimal(18,2) , sum(a4)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%'
    from
    (
	select  CourseId ,
	(case when Score>=85 then 1 else 0 end) a1,
	(case when Score<85 and Score >=70  then 1 else 0 end) a2,
	(case when Score<70 and Score >=60  then 1 else 0 end) a3,
	(case when Score<60 and Score >=0  then 1 else 0 end) a4
	from StudentCourseScore
	where CourseId=2
    ) a
    group by CourseId
    union 
    select CourseId , sum(a1)[100-85],sum(a2) [100-85],sum(a3)[100-85],sum(a4) [100-85],
    sum(a1)+sum(a2)+sum(a3)+sum(a4)总数,
    convert (nvarchar(50),(convert (decimal(18,2) , sum(a1)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%',
    convert (nvarchar(50),(convert (decimal(18,2) ,sum(a2)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%',
    convert (nvarchar(50),(convert (decimal(18,2) , sum(a3)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%',
    convert (nvarchar(50),(convert (decimal(18,2) , sum(a4)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%'
    from
    (
	select  CourseId ,
	(case when Score>=85 then 1 else 0 end) a1,
	(case when Score<85 and Score >=70  then 1 else 0 end) a2,
	(case when Score<70 and Score >=60  then 1 else 0 end) a3,
	(case when Score<60 and Score >=0  then 1 else 0 end) a4
	from StudentCourseScore
	where CourseId=3
    ) a
    group by CourseId
    union 
    select CourseId , sum(a1)[100-85],sum(a2) [100-85],sum(a3)[100-85],sum(a4) [100-85],
    sum(a1)+sum(a2)+sum(a3)+sum(a4)总数,
    convert (nvarchar(50),(convert (decimal(18,2) , sum(a1)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%',
    convert (nvarchar(50),(convert (decimal(18,2) ,sum(a2)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%',
    convert (nvarchar(50),(convert (decimal(18,2) , sum(a3)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%',
    convert (nvarchar(50),(convert (decimal(18,2) , sum(a4)*100.0/(sum(a1)+sum(a2)+sum(a3)+sum(a4)))))+'%'
    from
    (
	select  CourseId ,
	(case when Score>=85 then 1 else 0 end) a1,
	(case when Score<85 and Score >=70  then 1 else 0 end) a2,
	(case when Score<70 and Score >=60  then 1 else 0 end) a3,
	(case when Score<60 and Score >=0  then 1 else 0 end) a4
	from StudentCourseScore
	where CourseId=4
    ) a
    group by CourseId