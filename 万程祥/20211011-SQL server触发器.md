# 20211011-SQL server触发器

# 触发器为特殊类型的存储过程，可在执行语言事件时自动生效。
# SQL Server 包括三种常规类型的触发器：DML 触发器、DDL 触发器和登录触发器。

```
对于INSERT 操作，inserted保留新增的记录，deleted无记录
对于DELETE 操作，inserted无记录，deleted保留被删除的记录
对于UPDATE操作，inserted保留修改后的记录，deleted保留修改前的记录
```

-- 触发器 是一种特殊存储过程 它不由用户直接手动执行，而是当一些事件发生后，自动被调用

-- 作用 1、维护数据的有效性和完整性 2、简化逻辑

/*
语法：

``` sql
create trigger <触发器名称>
on <数据表>
<after | instead of> <delete | insert | update>
as
begin

end

``` 

``` sql
create trigger tr_StudentInfoForUpdate
on StudentInfo
after 
  Update
as
  select * from StudentInfo

update StudentInfo set StudentName='zhangsan' where Id=1
``` 

```
create trigger  tr_StudentCourseScoreForUpdate
on StudentCourseScore
after
 Update
 as 
 begin 
 declare @Id int,@Score int
 select @Id=Id , @Score = Score from inserted
 if(@Score <=60)
	begin 
		update StudentCourseScore set score='-1' where Id=@Id
		select * from StudentCourseScore
		select * from inserted
		end
	else 
	 begin 
		update StudentCourseScore set score=@Score where Id=@Id
		select * from StudentCourseScore
		select * from inserted
		end
	end

Update StudentCourseScore set score=50 where Id=1
select * from StudentCourseScore

```