# 20211008-SQL server递归函数

```
----------sql server 递归查询----------
--查找上级所有节点
with uCte as
(
  select a.id,a.title,a.pid from tree_table a where id = 3--当前节点
  union all
  select k.id,k.title,k.pid from tree_table k
  inner join uCte c on c.pid = k.id
)
select * from uCte;
--查找上级所有节点
with dCte as
(
  select a.id,a.title,a.pid from tree_table a where id = 3--当前节点
  union all
  select k.id,k.title,k.pid from tree_table k
  inner join dCte c on c.id = k.pid
)
select * from dCte;
```

```
递归由两个部分组成：
1.初始条件：列出哪些个体属于一个给定的集合，
2.归纳条件：当在条件中列出的个体属于给定集合时，则另一些个体也属于该集合
```