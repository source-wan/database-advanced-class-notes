# 20211018-关于SQL SERVER事务的知识

# 事务是什么意思
```
简单来说:就是我要完成好几个事情,这几个事情必须都成功.只要期中有一个事情失败了,那么前面成功的事情都无效.
这也是面对部分业务时，事务的优点
```


    begin transaction     开启事务

    commit transaction    提交事务

    Vrollback transaction 回滚事务

# 四种事务类型
```
1. 自动提交事务：每条单独的语句都是一个事务。

2. 显式事务：每个事务均以BEGIN TRANSACTION语句显式开始，以COMMIT或ROLLBACK语句显式结束。

3. 隐式事务：在前一个事务完成时新事务隐式开始，但每个事务仍已COMMIT或ROLLBACK语句显式完成。

4. 批处理级事务：只能应用于多个活动结果集（MARS），在MARS会话中启动的T-SQL显式或隐式事务变为批处理级事务。当批处理完成时没有提交或回滚的批处理级事务自动由SQL Server进行回滚。
```