# SQL server替换型触发器和索引

## 三种触发器的区别
```

            insert                    update                   delete
inserted   新插入的记录情况       更新后的记录情况               无


deleted    无                     更新前的记录情况             删除前的记录情况


```


## 创建insert触发器
``` sql
alter trigger tr_StudentCourseScoreForInsert
on StudentCourseScore
after insert
as
begin
    select * from inserted
    select * from StudentCourseScore
    select * from deleted
end
go

insert into StudentCourseScore (StudentId,CourseId,Score) values (2,22,888)


```


## 创建delete触发器
``` sql

alter trigger tr_StudentCourseScoreForDelete
on StudentCourseScore
after delete
as
begin
    select * from insertedV
    select * from StudentCourseScore
    select * from deleted
end
go

delete from StudentCourseScore  where Id=20


```

## 创建instead of触发器
``` sql
-- instead of 是替换执行 insert update delete中的语句。
---增删改在instead of存在的时候，并不会真正被执行，真正执行的其实是被触发的那个触发器
create trigger tr_StudentCourseScoreForInsertOf
on StudentCourseScore
instead of insert
as
begin
    set nocount on
    declare @score int ,@studentId int,@courseId int
    select @studentId=StudentId,@courseId=CourseId, @score=Score from inserted
    if @score>=0 and @score<=100
        begin
            insert into StudentCourseScore (StudentId,CourseId,Score) values (@studentId,@courseId,@score)
        end
    else
        begin
            print '成绩不在0到100的范围内，请确认后重试'
        end

    set nocount off
end
go

insert into StudentCourseScore (StudentId,CourseId,Score) values (2,22,100)

```

## set nocount的用法
```
当 SET NOCOUNT 为 ON 时，不返回计数（表示受 Transact-SQL 语句影响的行数）。当 SET NOCOUNT 为 OFF 时，返回计数。

即使当 SET NOCOUNT 为 ON 时，也更新 @@ROWCOUNT 函数。

当 SET NOCOUNT 为 ON 时，将不给客户端发送存储过程中的每个语句的 DONE_IN_PROC 信息。当使用 Microsoft SQL Server 提供的实用工具执行查询时，在 Transact-SQL 语句（如 SELECT、INSERT、 UPDATE 和 DELETE）结束时将不会在查询结果中显示"nn rows affected"。

如果存储过程中包含的一些语句并不返回许多实际的数据，则该设置由于大量减少了网络流量，因此可显著提高性能。

SET NOCOUNT 设置是在执行或运行时设置，而不是在分析时设置。

权限
SET NOCOUNT 权限默认授予所有用户。
```
## 测试
``` sql
set nocount on

declare @id int ,@name nvarchar(10)
set @id=1
set @name='a'

select @id,@name

set nocount off

```

```

索引分成两类：聚集索引和非聚集索引

聚集索引：就是数据存储的顺序和索引的顺序一致
非聚集索引：数据存储的秦贵育和索引未必一致

create [unique] [clustered | nonclustered] index <索引的名称>
on 数据表(字段1 [asc | desc],字段2 [asc | desc])

```


## 创建函数，可以根据输入的参数，随机构造若干条数据，插入到指定的表
``` sql
alter PROC test @startdate DATETIME
AS
    DECLARE @enddate DATETIME
--SET @startdate = '2014-01-01'
    SET @enddate = DATEADD(year, 1, @startdate)
    SELECT  *
    FROM    ( SELECT TOP 100000
                        CONVERT(VARCHAR(10), DATEADD(dd, number, @startdate), 23) new_date
              FROM      master..spt_values
              WHERE     DATEDIFF(day, DATEADD(day, number, @startdate),
                                 @enddate) >= 0
                        AND number >= 0
                        AND type = 'p'
              ORDER BY  CHECKSUM(NEWID())
            ) a
    ORDER BY new_date



```







## 在练习2的基础上，对指定的表创建索引，观察索引对数据查询的影响

``` sql
create  index indexTest
on test(字段1 [asc | desc],字段2 [asc | desc])

```