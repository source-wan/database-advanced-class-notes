

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberActivityInfo')
            and   type = 'U')
   drop table MemberActivityInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberCard')
            and   type = 'U')
   drop table MemberCard
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberCardLossReortInfo')
            and   type = 'U')
   drop table MemberCardLossReortInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberCardTypeInfo')
            and   type = 'U')
   drop table MemberCardTypeInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberInfo')
            and   type = 'U')
   drop table MemberInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberLvelInfo')
            and   type = 'U')
   drop table MemberLvelInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberPointsExchangeInfo')
            and   type = 'U')
   drop table MemberPointsExchangeInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberPointsInfo')
            and   type = 'U')
   drop table MemberPointsInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MemberPointsRuleInfo')
            and   type = 'U')
   drop table MemberPointsRuleInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('SMInfo')
            and   type = 'U')
   drop table SMInfo
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ShopsInfo')
            and   type = 'U')
   drop table ShopsInfo
go

/*==============================================================*/
/* Table: MemberActivityInfo          会员活动信息表                           */
/*==============================================================*/
create table MemberActivityInfo (
   MemberActivityId     int                  not null,
   MemberId             int                  null,
   SMId                 int                  null,
   ActivityInfo         nvarchar(10)         null,
   ActivityTime         nvarchar(20)         null,
   constraint PK_MEMBERACTIVITYINFO primary key (MemberActivityId)
)
go

/*==============================================================*/
/* Table: MemberCard           会员卡信息表                                 */
/*==============================================================*/
create table MemberCard (
   MemberCardId         int                  not null,
   MembereCardNum       nvarchar(30)         null,
   MembereCardType      nvarchar(30)         null,
   SMId                 int                  null,
   MemberId             int                  null,
   constraint PK_MEMBERCARD primary key (MemberCardId)
)
go

/*==============================================================*/
/* Table: MemberCardLossReortInfo        会员卡挂失信息表                        */
/*==============================================================*/
create table MemberCardLossReortInfo (
   LoseId               int                  not null,
   MemberId             int                  null,
   SMId                 int                  null,
   LoseTime             datetime             null,
   constraint PK_MEMBERCARDLOSSREORTINFO primary key (LoseId)
)
go

/*==============================================================*/
/* Table: MemberCardTypeInfo            会员卡类型信息表                        */
/*==============================================================*/
create table MemberCardTypeInfo (
   MemberCardTypeId     int                  not null,
   MemberCardNum        nvarchar(10)         null,
   MemberCardType       nvarchar(10)         null,
   MemberId             int                  null,
   constraint PK_MEMBERCARDTYPEINFO primary key (MemberCardTypeId)
)
go

/*==============================================================*/
/* Table: MemberInfo              会员信息表                              */
/*==============================================================*/
create table MemberInfo (
   MemberId             int                  not null,
   MemberName           nvarchar(20)         null,
   Sex                  nvarchar(2)          null,
   Birthday             nvarchar(10)         null,
   Tel                  nvarchar(20)         null,
   IdCardNum            nvarchar(20)         null,
   Address              nvarchar(80)         null,
   constraint PK_MEMBERINFO primary key (MemberId)
)
go

/*==============================================================*/
/* Table: MemberLvelInfo               会员等级信息表                         */
/*==============================================================*/
create table MemberLvelInfo (
   MemberLevelId        int                  not null,
   MemberId             int                  null,
   MemberLevel          nvarchar(10)         null,
   constraint PK_MEMBERLVELINFO primary key (MemberLevelId)
)
go

/*==============================================================*/
/* Table: MemberPointsExchangeInfo         会员积分兑换表                     */
/*==============================================================*/
create table MemberPointsExchangeInfo (
   PointsExchange       int                  not null,
   MemberId             int                  null,
   SMId                 int                  null,
   ExchangeTime         datetime             null,
   constraint PK_MEMBERPOINTSEXCHANGEINFO primary key (PointsExchange)
)
go

/*==============================================================*/
/* Table: MemberPointsInfo             会员积分信息表                         */
/*==============================================================*/
create table MemberPointsInfo (
   MemberPointsId       int                  not null,
   MemberId             int                  null,
   MemberCardNum        nvarchar(10)         null,
   SMId                 int                  null,
   constraint PK_MEMBERPOINTSINFO primary key (MemberPointsId)
)
go

/*==============================================================*/
/* Table: MemberPointsRuleInfo        会员积分规则表                          */
/*==============================================================*/
create table MemberPointsRuleInfo (
   MemberPointsRuleId   int                  not null,
   MemberPointsRule     varchar(80)          null,
   SMId                 int                  null,
   constraint PK_MEMBERPOINTSRULEINFO primary key (MemberPointsRuleId)
)
go

/*==============================================================*/
/* Table: SMInfo                           商场信息表                     */
/*==============================================================*/
create table SMInfo (
   SMId                 int                  not null,
   SMName               nvarchar(10)         null,
   SMAbbreviation       nvarchar(10)         null,
   SMAddress            nvarchar(80)         null,
   SMIps                nvarchar(10)         null,
   IpsTel               nvarchar(30)         null,
   constraint PK_SMINFO primary key (SMId)
)
go

/*==============================================================*/
/* Table: ShopsInfo                   店铺信息表                          */
/*==============================================================*/
create table ShopsInfo (
   ShopsId              int                  not null,
   ShopesName           nvarchar(30)         null,
   SMId                 int                  null,
   ShopsPosition        nvarchar(30)         null,
   ShopesOperator       nvarchar(30)         null,
   OperatorTel          nvarchar(20)         null,
   constraint PK_SHOPSINFO primary key (ShopsId)
)
go

/*==============================================================*/
/* Table: UserInfo                   用户信息表                          */
/*==============================================================*/
create table UserInfo
(
	Id int not null identity primary key,
	Name nvarchar(10) not null unique,
	Password  nvarchar(10) not null 
)

--注册
insert into UserInfo (Name,Password)
values  ('admin','123')

--批量注册
insert into UserInfo (Name,Password)
values
('admin01','124'),('admin02','125'),('admin03','126');

--登录

--根据提供的用户名和密码 查询是否存在满足条件的记录，有则登录成功，否则登录失败
select * from UserInfo
where Name='admin' and Password='123'

--更进一步，就是当前登录的用户可能已经被注销，禁用等情况，如何应对

delete from UserInfo where Name = 'admin02' 

--判断当前注册的用户名是否已经存在，是则不允许再注册，返回注册失败信息；否则可以继续往下走
declare @username nvarchar(10)
declare @tmpTable1 table (id int,Username nvarchar(80),Password nvarchar(80))
declare @count1 int 

insert into @tmpTable1
select @count1 = count(*) from UserInfo where Name=@username

/*
if(@count1 >0)
	begin

	--做点什么 如提示消息或者设置一些数值

	end 
 else 
	begin

	end
*/

---判断密码和重复密码是否一致，是否可以注册，并在相应数据表中插入一条记录，否则返回失败信息
--declare @password nvarchar(10)
--declare @tmpTable2 table (id int,Username nvarchar(80),Password nvarchar(80))
--declare @count2 int 

--insert into @tmpTable1
--select @count2 = count(*) from UserInfo where Password=@password

/*
if(@count >0)
	begin
insert into UserInfo (Name,Password)
values （' ' , ' '）

	end 

 else 
	begin

	end
*/

----关于商场信息的使用场景

----在会员信息管理的时候，需要选择商场
--select SMId,SMName,SMAbbreviation  from SMInfo

----在会员卡类型管理的时候，需要选择商场
--select SMId,SMName,SMAbbreviation  from SMInfo

----商场信息 的数据增删改查
--insert into SMInfo (SMName,SMAbbreviation,SMAddress,SMIps,IpsTel) values ( )
--delete from SMInfo where 
--update  SMInfo set   where 
--select * from SMInfo

----关于店铺信息的使用场景
----在积分管理的时候，需要选择店铺
--select ShopsId,ShopsName  from ShopsInfo

----店铺信息 的数据增删改查
--insert into ShopsInfo (ShopsName,SMId,ShopsPosition,ShopesOperator,OperatorTel) values ( )
--delete from ShopsInfo where 
--update  ShopsInfo  set   where 
--select * from ShopsInfo

----关于会员信息的使用场景
----在积分管理的时候，需要选择会员
--select MemberId,MemberName  from MemberInfo

----在会员卡类型信息的时候，需要选择会员
--select MemberId,MemberName  from MemberInfo

----在会员卡挂失信息表的时候，需要选择会员
--select MemberId,MemberName  from MemberInfo


----会员信息 的数据增删改查
--insert into MemberInfo (MemberName,Sex,Birthday,Tel,IdCardNum,Address) values ( )
--delete from MemberInfo where 
--update  MemberInfo  set   where 
--select * from MemberInfo

declare @password nvarchar(80),@rowPassword nvarchar(80)
declare @isDeleted bit ,@isActived bit
declare @count int
declare @res nvarchar(80)


select @count=COUNT(*),@isDeleted=IsDeleted,@isActived=IsActived,@rowPassword=Password from Users 
where Username=@username

-- 如果根据提供的用户名，找到的记录数不为零，则表明用户名是对的，可以继续往下判断；否则直接提示用户名或密码错误
if(@count>0)
	begin
		if(@isDeleted=1) --如果删除标记为真，则表明当前用户已经被删除，作出提示；否则用户未被删除
			begin
				set @res='当前用户已经被删除，有任何问题请联系管理员@_@'
			end
		else
			begin
				if(@isActived=0) --如果当前用户未启用/激活，则提示信息用户未启用，登录失败；否则用户已经禁用
					begin
						set @res='用户已禁用，有任何问题请联系管理员'
					end
				else
					begin
						if(@password=@rowPassword) -- 如果传进来的密码和在记录中获取的密码相等，则认为登录成功，作出提示；否则登录失败，提示相应信息
							begin
								set @res='登录成功'
							end
						else
							begin
								set @res='用户名或者密码错误，请确认后重试。。。'
							end
					end

			end

	end
else
	begin
		set @res='用户名或者密码错误，请确认后重试。。。'
	end


